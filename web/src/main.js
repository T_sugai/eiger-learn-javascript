'use strict';
import { HttpRequest } from './HttpRequest';

// プログラミングでやることは
// 順次処理、繰り返し、条件分岐
// 即時関数

/**
 * liに設定されている、class名を取得して、対応するID名のwindowを開く。
 * @param {String} className
 * @returns {void}
 */

function openWindow(className) {
  const html = document.getElementsByClassName(className);
  //処理
  Object.values(html).forEach((list, index) => {
    list.addEventListener('click', () => {
      const elm = document.getElementById('window' + (index + 1));
      elm.classList.add('active');
    });
  });
}

const currentPathname = window.location.pathname;

//三項演算子
//const 変数　＝　条件　？　true : false

const clickElm = currentPathname === '/under.html' ? 'click' : 'list';

openWindow(clickElm);

// YAGNI原則  You Ain't Gonna Need It
//　KISS原則  Keep It Simple Stupid

const json = [
  {
    'Lang name': 'Python',
    description:
      'Python（パイソン）は、汎用のプログラミング言語である。コードがシンプルで扱いやすく設計されており、C言語などに比べて、さまざまなプログラムを分かりやすく、少ないコード行数で書けるといった特徴がある。',
    links: [
      'https://www.python.org/',
      'https://www.katacoda.com/courses/python/playground',
      'https://github.com/python',
    ],
  },

  {
    'Lang name': 'Golang',
    description:
      'Goは、静的型付け、C言語の伝統に則ったコンパイル言語、メモリ安全性、ガベージコレクション、構造的型付け（英語版）、CSPスタイルの並行性などの特徴を持つ。Goのコンパイラ、ツール、およびソースコードは、すべてフリーかつオープンソースである。',
    links: [
      'https://golang.org/',
      'https://go-tour-jp.appspot.com/welcome/1',
      'https://github.com/golang/go',
    ],
  },
  {
    'Lang name': 'Kotlin',
    description:
      'Kotlin言語は、ロシア連邦レニングラード州都のサンクトペテルブルクにある、ジェットブレインズ社の研究所で生まれた。ジェットブレインズ社は Java、Ruby、Python などのプログラミング言語による開発環境などを開発して販売してきた。Kotlin言語は、同社の経験を活かしJava言語をもっと簡潔・安全になるように改良した産業利用向け汎用言語として開発され、2011年7月20日に発表された。',
    links: [
      'https://developer.android.com/kotlin?hl=ja',
      'https://play.kotlinlang.org/',
      'https://github.com/JetBrains/kotlin',
    ],
  },
];

/**
 * modal.htmlに各言語の名前、説明、リンクを動的に生成する関数。
 *　加藤くんに教えてもらったinsertAjuacentHtml()を利用してみました。
 * バリ便利やわ
 *
 * @returns {void}
 */
const elmModal = document.getElementById('modal');

json.forEach(elm => {
  const linkRenderer = elm.links.map(
    (link, index) =>
      `<a href=${link} target="_blank">${elm['Lang name']}:link${index}</a>`,
  );
  elmModal.insertAdjacentHTML(
    'beforeend',
    `Lang name:${elm['Lang name']}<br/>description:${elm.description}<br/>links:${linkRenderer}<br/><br/>`,
  );
});

HttpRequest();
