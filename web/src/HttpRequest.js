import axios from 'axios';

export const HttpRequest = () =>
  axios
    .get('https://www.land.mlit.go.jp/webland/api/CitySearch?area=13') // (resolve,reject)=>{}
    // GETがうまくいった場合の動作を記述

    .then(response => {
      //  console.log('status:', response.status);
      const data = response.data;
      console.log(data);
    })
    // HTTP通信が失敗した場合の動作を記述
    .catch(err => {
      console.log('err:', err);
    })
    // 成功失敗に限らず行いたい動作を記述
    .finally(() => {
      console.log('GET Operation is end.');
    });
